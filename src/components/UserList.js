import React from "react";
import "./userList.css";

const UserList = () => {
  const users = [
    { id: 1, name: "Sam Smith" },
    { id: 2, name: "Lisa More" },
    { id: 3, name: "David Cohen" },
    { id: 4, name: "Jim Taylor" },
  ];

  const renderedUsers = users.map(({ id, name }) => (
    <li key={id} className="list-item">
      {name}
    </li>
  ));

  return <ul className="list">{renderedUsers}</ul>;
};

export default UserList;
