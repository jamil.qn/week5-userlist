import React from "react";
import UserList from "./UserList";
import "./userList.css";


const App = () =>{
    return (
        <div className="list-container">
            <UserList />
        </div>
    )
}

export default App;